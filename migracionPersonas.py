import conexion
import consultas
import uuid
import time
import log
from tqdm import tqdm

lenBuffer = 100

### Tablas de MajomaControlHospitalario en los que supongo son personas
# Medico
# Paciente

def strParaConsulta(cadena):
    return "'" + cadena + "'"

def procesarFilaMedico(fila,tabla_origen,contador):
    try:
        nombre_persona = {}
        if " " in fila[1]:
            nombre_persona["PN"] = "'" + fila[1].split(" ")[0].strip("'") + "'"
            nombre_persona["SN"] = "'" + fila[1].split(" ")[1].strip("'") + "'"
        else:
            nombre_persona["PN"] = "'" + fila[1] + "'"
        if " " in fila[2]:
            nombre_persona["PA"] = "'" + fila[2].split(" ")[0].strip("'") + "'"
            nombre_persona["SA"] = "'" + fila[2].split(" ")[1].strip("'") + "'"
        else:
            nombre_persona["PA"] = "'" + fila[2] + "'"

        idPersona = 0
        #Inserto persona del usuario en tabla person
        if consultas.nombrePersonaExiste(nombre_persona["PN"],nombre_persona.get("SN","''"),nombre_persona["PA"],nombre_persona.get("SA","''")):
            idPersona = consultas.id_persona(nombre_persona["PN"],nombre_persona.get("SN","''"),nombre_persona["PA"],nombre_persona.get("SA","''"))
            datosPersona = consultas.obtenerInfoPorId("person",idPersona)

            if str(fila[6]).lower() == "m":
                datosPersona["gender"] = "'MASCULINO'"
            elif str(fila[6]).lower() == "f":
                datosPersona["gender"] = "'FEMENINO'"
            else:
                datosPersona["gender"] ="'INDETERMINADO'"

            idPersona = consultas.actualizarEnTabla("person",idPersona,datosPersona,tabla_origen,contador)

        else:
            tablaPerson = consultas.obtener_formato_tabla_openMRS("person")
            tablaPerson["uuid"] = "'" + str(uuid.uuid1()) + "'"
            tablaPerson["date_created"] = "'" + str(fila[13]) + "'"
            if str(fila[6]).lower() == "m":
                tablaPerson["gender"] = "'MASCULINO'"
            elif str(fila[6]).lower() == "f":
                tablaPerson["gender"] = "'FEMENINO'"
            else:
                tablaPerson["gender"] ="'INDETERMINADO'"

            idPersona = consultas.insertarEnTabla("person",tablaPerson,tabla_origen,contador)
            
            if idPersona == 0:
                print("Error dentro de NO EXISTE")
                return False

            tablaPersonName = consultas.obtener_formato_tabla_openMRS("person_name")
            tablaPersonName["uuid"] = "'" + str(uuid.uuid1()) + "'"
            tablaPersonName["date_created"] = "'" + str(fila[13]) + "'"
            tablaPersonName["person_id"] = idPersona
            tablaPersonName["given_name"] = nombre_persona.get("PN", "''")
            tablaPersonName["middle_name"] = nombre_persona.get("SN", "''")
            tablaPersonName["family_name"] = nombre_persona.get("PA", "''")
            tablaPersonName["family_name2"] = nombre_persona.get("SA", "''")
            tablaPersonName["creator"] = 1
            #print(tablaPersonName)
            consultas.insertarEnTabla("person_name",tablaPersonName,tabla_origen,contador)
            #print("Termina rama NO EXISTE")

        if idPersona == 0:
            print("ERROR FUERA IF ELSE")
            return False

        tablaPersonAddress = consultas.obtener_formato_tabla_openMRS("person_address")
        tablaPersonAddress["person_id"] = idPersona
        tablaPersonAddress["address1"] = "'" + fila[4] + "'"
        tablaPersonAddress["creator"] = 1
        tablaPersonAddress["date_created"] = "'" + str(fila[13]) + "'"
        tablaPersonAddress["uuid"] = "'" + str(uuid.uuid1()) + "'"
        consultas.insertarEnTabla("person_address", tablaPersonAddress,tabla_origen,contador)

        archivoAtributos = open("./Mapeos/mapeoAtributosMedico-AtributosPersonas.csv")
        archivoAtributos.readline()
        linea = archivoAtributos.readline()
        tablaPersonAttribute = consultas.obtener_formato_tabla_openMRS("person_attribute")
        while linea:
            #print("entró al while")
            posicion = int(linea.split(";")[0])
            atributo = ((linea.strip("\n")).split(";")[1]).upper()
            tablaPersonAttribute["person_id"] = idPersona
            tablaPersonAttribute["person_attribute_type_id"] = consultas.id_tipo_atributo_persona(atributo)
            tablaPersonAttribute["value"] = "'" + str(fila[posicion]) + "'"
            tablaPersonAttribute["uuid"] = "'" + str(uuid.uuid1()) + "'"
            tablaPersonAttribute["creator"] = 1
            tablaPersonAttribute["date_created"] = "'" + str(fila[13]) + "'"
            tablaPersonAttribute["voided"] = 0
            consultas.insertarEnTabla("person_attribute", tablaPersonAttribute,tabla_origen,contador)
            linea = archivoAtributos.readline()
        archivoAtributos.close()

        #print("Sale del while del mapeoAtributosMedico-AtributosPersonas")

        #Aqui comienza la creacion del provider por medico
        tablaProvider = consultas.obtener_formato_tabla_openMRS("provider")
        tablaProvider["person_id"] = idPersona
        tablaProvider["identifier"] = "'" + fila[3] + "'"
        tablaProvider["creator"] = 1
        tablaProvider["date_created"] = "'" + str(fila[13]) + "'"
        tablaProvider["retired"] = 0
        tablaProvider["uuid"] = "'" + str(uuid.uuid1()) + "'"
        idProvider = consultas.insertarEnTabla("provider",tablaProvider,tabla_origen,contador)
        if idProvider== 0:
            return False
        consultas.insertarEnidMedico_idProvider(int(fila[0]),int(idProvider))

        archivoAtributos = open("./Mapeos/mapeoAtributosMedico-AtributosProvider.csv")
        archivoAtributos.readline()
        linea = archivoAtributos.readline()
        while linea:
            posicion = int(linea.split(";")[0])
            atributo = linea.split(";")[1].strip("\n").upper()
            #print(posicion,atributo)
            tablaProviderAttribute = consultas.obtener_formato_tabla_openMRS("provider_attribute")
            tablaProviderAttribute["provider_id"] = idProvider
            tablaProviderAttribute["attribute_type_id"] = consultas.id_tipo_atributo_provider(atributo)
            tablaProviderAttribute["value_reference"] = "'" + str(fila[posicion]) + "'"
            tablaProviderAttribute["uuid"] = "'" + str(uuid.uuid1()) + "'"
            tablaProviderAttribute["creator"] = 1
            tablaProviderAttribute["date_created"] = "'" + str(fila[13]) + "'"
            tablaProviderAttribute["voided"] = 0
            consultas.insertarEnTabla("provider_attribute",tablaProviderAttribute,tabla_origen,contador)
            linea = archivoAtributos.readline()

        #print("Termina de insertar")
        return True

    except Exception as e:
        log.adding_log(tabla_origen,"person-provider",contador,e)
        #print(e)
        return False

def procesarFilaPaciente(fila,tabla_origen,contador):
    try:
        idPersona = 0
        if consultas.nombrePersonaExiste(strParaConsulta(fila[3].strip("'")),strParaConsulta(fila[4].strip("'")),strParaConsulta(fila[5].strip("'")),strParaConsulta(fila[6].strip("'"))):
            idPersona = consultas.id_persona(strParaConsulta(fila[3].strip("'")),strParaConsulta(fila[4].strip("'")),strParaConsulta(fila[5].strip("'")),strParaConsulta(fila[6].strip("'")))
            datosPersona = consultas.obtenerInfoPorId("person",idPersona)

            if str(fila[7]).lower() == "m":
                datosPersona["gender"] = "'MASCULINO'"
            elif str(fila[7]).lower() == "f":
                datosPersona["gender"] = "'FEMENINO'"
            else:
                datosPersona["gender"] ="'INDETERMINADO'"

            idPersona = consultas.actualizarEnTabla("person",idPersona,datosPersona,tabla_origen,contador)
        else:
            tablaPerson = consultas.obtener_formato_tabla_openMRS("person")

            tablaPerson["uuid"] = "'" + str(uuid.uuid1()) + "'"
            tablaPerson["date_created"] = "'" + str(fila[31]) + "'"

            if str(fila[7]).lower() == "m":
                tablaPerson["gender"] = "'MASCULINO'"
            elif str(fila[7]).lower() == "f":
                tablaPerson["gender"] = "'FEMENINO'"
            else:
                tablaPerson["gender"] ="'INDETERMINADO'"
            
            idPersona = consultas.insertarEnTabla("person",tablaPerson,tabla_origen,contador)

            if idPersona == 0:
                return False

            consultas.insertarEnidPaciente_idPatient(int(fila[0]),int(idPersona))

            tablaPersonName = consultas.obtener_formato_tabla_openMRS("person_name")
            tablaPersonName["uuid"] = "'" + str(uuid.uuid1()) + "'"
            tablaPersonName["date_created"] = "'" + str(fila[31]) + "'"
            tablaPersonName["person_id"] = idPersona
            tablaPersonName["given_name"] = "'" + fila[3] + "'"
            tablaPersonName["middle_name"] = "'" + fila[4] + "'"
            tablaPersonName["family_name"] = "'" + fila[5] + "'"
            tablaPersonName["family_name2"] = "'" + fila[6] + "'"
            tablaPersonName["creator"] = 1
            consultas.insertarEnTabla("person_name",tablaPersonName,tabla_origen,contador)

            tablaPersonAddress = consultas.obtener_formato_tabla_openMRS("person_address")
            tablaPersonAddress["person_id"] = idPersona
            tablaPersonAddress["address1"] = "'" + fila[17] + "'"
            tablaPersonAddress["creator"] = 1
            tablaPersonAddress["date_created"] = "'" + str(fila[31]) + "'"
            tablaPersonAddress["uuid"] = "'" + str(uuid.uuid1()) + "'"
            consultas.insertarEnTabla("person_address", tablaPersonAddress,tabla_origen,contador)

        if idPersona == 0:
            return False
            
        if not consultas.idPersonaEnPatients(idPersona):
            tablaPatient = consultas.obtener_formato_tabla_openMRS("patient")
            tablaPatient["patient_id"] = idPersona
            tablaPatient["creator"] = 1
            tablaPatient["date_created"] = "'" + str(fila[31]) + "'"
            tablaPatient["voided"] = int(tablaPatient["voided"])
            tablaPatient["allergy_status"] = strParaConsulta(tablaPatient["allergy_status"])

            idPatient = consultas.insertarEnTabla("patient", tablaPatient,tabla_origen,contador)
            #consultas.insertarEnidPaciente_idPatient(int(fila[0]),int(idPersona))

        # Falta ver lo del patient identifier.
        
        return True

    except Exception as e:
        log.adding_log(tabla_origen,"person-patient",contador,e)
        return False

def main(prueba=False):
    #Leyendo la tabla tbMedico    
    tablaAMigrar = "tbMedico"
    cantFilasTablaAMigrar = consultas.numeroColumnas(tablaAMigrar)
    if prueba and cantFilasTablaAMigrar>1000:
        cantFilasTablaAMigrar = 1000
    contFilas = 0
    print("Migrando la tabla " + tablaAMigrar)
    barraProgresoMedicos = tqdm(range(cantFilasTablaAMigrar))
    while contFilas < cantFilasTablaAMigrar:
        query = "select * from (select *,ROW_NUMBER() over (order by(select NULL) desc) as numfila from " + tablaAMigrar + ")t where numfila > " + str(contFilas) + " and numfila < " + str(contFilas + lenBuffer + 1)
        CSR_MAJOMA = conexion.getConexionMCH().cursor()
        CSR_MAJOMA.execute(query)
        row = CSR_MAJOMA.fetchone()
        while row:
            if not procesarFilaMedico(row,tablaAMigrar,contFilas+1):
                print("Hubo un error al procesar la fila", contFilas+1)
            row = CSR_MAJOMA.fetchone()
            contFilas += 1
            barraProgresoMedicos.update(1)
            barraProgresoMedicos.refresh()
        CSR_MAJOMA.close()
    barraProgresoMedicos.close()
    
    #Leyendo la tabla tbPaciente
    tablaAMigrar = "tbPaciente"
    cantFilasTablaAMigrar = consultas.numeroColumnas(tablaAMigrar)
    if prueba and cantFilasTablaAMigrar>1000:
        cantFilasTablaAMigrar = 1000
    contFilas = 0
    print("Migrando la tabla " + tablaAMigrar)
    barraProgresoPacientes = tqdm(range(cantFilasTablaAMigrar))
    while contFilas < cantFilasTablaAMigrar:
        query = "select * from (select *,ROW_NUMBER() over (order by(select NULL) desc) as numfila from " + tablaAMigrar + ")t where numfila > " + str(contFilas) + " and numfila < " + str(contFilas + lenBuffer + 1)
        CSR_MAJOMA = conexion.getConexionMCH().cursor()
        CSR_MAJOMA.execute(query)
        row = CSR_MAJOMA.fetchone()
        while row:
            if not procesarFilaPaciente(row,tablaAMigrar,contFilas+1):
                print("Hubo un error al procesar la fila", contFilas+1)
            row = CSR_MAJOMA.fetchone()
            contFilas += 1
            barraProgresoPacientes.update(1)
            barraProgresoPacientes.refresh()
        CSR_MAJOMA.close()
        del(CSR_MAJOMA)
    barraProgresoPacientes.close()
        

    print("TERMINADO LA MIGRACIÓN DE PERSONAS")
