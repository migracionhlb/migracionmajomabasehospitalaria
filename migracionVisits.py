import conexion
import consultas
import uuid
import time
import log
from tqdm import tqdm

rutaArchivoMapeo = "./Mapeos/columnasAConceptos.csv"
lenBuffer = 100
#COMENTARIO PARA QUE VUELVA AL REPO EN LA MV
diccionarioMapeo = dict()

def sacarInfoArchivoMapeo():
	archivo = open(rutaArchivoMapeo,encoding="latin-1")
	archivo.readline()
	linea = archivo.readline()
	lista = []
	while linea:
		#print(linea)
		sep = linea.strip("\n").split(";")
		if sep[0] not in diccionarioMapeo:
			diccionarioMapeo[sep[0]] = dict()
		diccionarioMapeo[sep[0]][sep[2]] = dict()
		diccionarioMapeo[sep[0]][sep[2]]["posicion"] = int(sep[1])
		diccionarioMapeo[sep[0]][sep[2]]["tipoDato"] = sep[3]
		diccionarioMapeo[sep[0]][sep[2]]["observacion"] = sep[4]
		linea = archivo.readline()
	archivo.close()
	
def migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contador):
	try:
		if idPatient != 0 and idProvider != 0 and idVisita != 0:
			diccionarioEncounter = consultas.obtener_formato_tabla_openMRS("encounter")
			diccionarioEncounter["encounter_type"] = 3
			diccionarioEncounter["visit_id"] = idVisita
			diccionarioEncounter["location_id"] = 10 #Es el id del lugar donde se realiza la atención de emergencia.
			diccionarioEncounter["form_id"]	= 8 #Es el id del formulario 008 en la tabla form
			diccionarioEncounter["encounter_datetime"] = "'" + fecha + "'"
			diccionarioEncounter["creator"] = 1
			diccionarioEncounter["date_created"] = "'" + fecha + "'"
			diccionarioEncounter["uuid"] = "'" + str(uuid.uuid1()) + "'"
			diccionarioEncounter["patient_id"] = idPatient
			idEncounter = consultas.insertarEnTabla("encounter",diccionarioEncounter,tablaAMigrar,contador)
			if idEncounter == 0:
				log.adding_log(tablaAMigrar,"Obs-Encounter",contador,"idEncounter == 0")
				return False

			diccionarioEncounter_Provider = consultas.obtener_formato_tabla_openMRS("encounter_provider")
			diccionarioEncounter_Provider["encounter_id"] = idEncounter
			diccionarioEncounter_Provider["provider_id"] = idProvider
			diccionarioEncounter_Provider["encounter_role_id"] = 2 #Es el id para todos los que son Doctores o enfermeros
			diccionarioEncounter_Provider["creator"] = 1
			diccionarioEncounter_Provider["date_created"] = "'" + fecha + "'"
			diccionarioEncounter_Provider["uuid"] = "'" + str(uuid.uuid1()) + "'"
			idEncounter_Provider = consultas.insertarEnTabla("encounter_provider",diccionarioEncounter_Provider,tablaAMigrar,contador)
			if idEncounter_Provider == 0:
				log.adding_log(tablaAMigrar,"Obs-Encounter",contador,"idEncounter_Provider == 0")
				return False

			#Viene la groupingObservation por el encounter
			diccionarioObs = consultas.obtener_formato_tabla_openMRS("obs")
			diccionarioObs["person_id"] = idPatient
			diccionarioObs["concept_id"] = consultas.idConceptoPorNombre("EMERGENCIA")
			diccionarioObs["encounter_id"] = idEncounter
			diccionarioObs["obs_datetime"] = "'" + fecha + "'"
			diccionarioObs["location_id"] = 10
			diccionarioObs["creator"] = 1
			diccionarioObs["uuid"] = "'" + str(uuid.uuid1()) + "'"
			diccionarioObs["status"] = "'" + "FINAL" + "'"
			diccionarioObs["date_created"] = "'" + fecha + "'"
			idGroupingObs = consultas.insertarEnTabla("obs",diccionarioObs,tablaAMigrar,contador)
			if idGroupingObs == 0:
				log.adding_log(tablaAMigrar,"Obs-Encounter",contador,"idGroupingObs==0")
				return False

			#Aquí va un ciclo de todas las observations que se deben insertar.
			conceptosTabla = diccionarioMapeo[tablaAMigrar]
			#print(conceptosTabla)
			for key,value in conceptosTabla.items():
				if fila[value["posicion"]] is not None:
					idConcepto = consultas.idConceptoPorNombre((prefijo+key).upper())
					diccionarioObs = consultas.obtener_formato_tabla_openMRS("obs")
					diccionarioObs["person_id"] = idPatient
					diccionarioObs["concept_id"] = idConcepto
					diccionarioObs["encounter_id"] = idEncounter
					diccionarioObs["obs_datetime"] = "'" + fecha + "'"
					diccionarioObs["location_id"] = 10
					diccionarioObs["creator"] = 1
					diccionarioObs["obs_group_id"] = idGroupingObs
					diccionarioObs["uuid"] = "'" + str(uuid.uuid1()) + "'"
					diccionarioObs["status"] = "'" + "FINAL" + "'"
					diccionarioObs["date_created"] = "'" + fecha + "'"
					diccionarioObs["value_text"] = "'" + str(fila[value["posicion"]]) + "'"
					idObs = consultas.insertarEnTabla("obs",diccionarioObs,tablaAMigrar,contador)
					if idObs == 0:
						log.adding_log(tablaAMigrar,"Obs-Encounter",contador,"idObs==0")
						return False
			return True
		else: #Se espera que ocurra solo en periodo de pruebas
			cadena = ""
			if idPatient == 0:
				cadena += "Paciente con id=0,"
			if idProvider == 0:
				cadena += "Provider con id=0,"
			if idVisita == 0:
				cadena += "Visita con id=0,"
			log.adding_log(tablaAMigrar,"Obs-Encounter",contador,cadena[:-1])

			return False
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contador,e)
		return False

def procesarFilatb1RegistroAdmision(fila,tablaAMigrar,contador):
	try:
		idPatient = consultas.idPatientPorIdPaciente(int(fila[3]))
		idProvider = consultas.idProviderPorIdMedico(int(fila[8]))
		fecha = str(fila[13])
		prefijo = "tb1"
		
		if idPatient != 0 and idProvider != 0 and consultas.nombreConceptoExiste("EMERGENCIA"):
			#print("Entra al if de función")
			diccionarioVisit = consultas.obtener_formato_tabla_openMRS("visit")
			diccionarioVisit["patient_id"] = idPatient
			diccionarioVisit["visit_type_id"] = 1
			diccionarioVisit["date_started"] = "'" + fecha + "'"
			diccionarioVisit["indication_concept_id"] = consultas.idConceptoPorNombre("EMERGENCIA")
			diccionarioVisit["location_id"] = 10 #Es el id del lugar donde se realiza la atención de emergencia.
			diccionarioVisit["creator"] = 1
			diccionarioVisit["date_created"] = "'" + fecha + "'"
			diccionarioVisit["uuid"] = "'" + str(uuid.uuid1()) + "'"
			idVisita = consultas.insertarEnTabla("visit",diccionarioVisit,tablaAMigrar,contador)
			if idVisita != 0:
				consultas.insertarEnCodigoIngreso_idVisit(int(fila[0]),idVisita)
				if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contador):
					return False

		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contador,e)
		return False

def procesarFilatb2InicioAtencionMotivo(fila,tablaAMigrar,contador):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[3])
		idProvider = consultas.idProviderPorIdMedico(fila[2])
		fecha = str(fila[5])
		prefijo = "tb2"
		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contador):
			return False

		#print("Fila de Tb2 migrada correctamente")
		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contador,e)

		return False

def procesarFilatb3AccidenteViolenciaIntoxicacionEnvenenamientoQuemadura(fila,tablaAMigrar,contador):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		fecha = str(fila[5])
		prefijo = "tb3"

		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contador):
			return False

		#print("Fila de Tb3 migrada correctamente")
		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contador,e)

		return False

def procesarFilatb4EmergenciaAntecedentesFamiliaresPersonales(fila,tablaAMigrar,contador):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		fecha = str(fila[5])
		prefijo = "tb4"

		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contador):
			return False

		#print("Fila de Tb4 migrada correctamente")
		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contador,e)

		return False

def procesarFilatb5EmergenciaEnfermedadActual(fila,tablaAMigrar,contador):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		fecha = str(fila[5])
		prefijo = "tb5"

		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contador):
			return False

		#print("Fila de Tb5 migrada correctamente")
		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contador,e)

		return False

def migrarObservacion(tablaAMigrar,funcion,prueba=False):
	if not prueba:
		cantFilasTablaAMigrar = consultas.numeroColumnas(tablaAMigrar)
	else:
		cantFilasTablaAMigrar = 1000
	print("Migrando tabla", tablaAMigrar)
	
	contFilas = 0

	barraProgreso = tqdm(range(cantFilasTablaAMigrar))

	while contFilas < cantFilasTablaAMigrar:
		#print("Entra al while")
		query = "select * from (select *,ROW_NUMBER() over (order by(select NULL) desc) as numfila from " + tablaAMigrar + ")t where numfila > " + str(contFilas) + " and numfila < " + str(contFilas + lenBuffer + 1)
		CSR_MAJOMA = conexion.getConexionMCH().cursor()
		CSR_MAJOMA.execute(query)
		row = CSR_MAJOMA.fetchone()
		while row:
			if not funcion(row,tablaAMigrar,contFilas+1):
				log.adding_log(tablaAMigrar,"migrarObservacion",contFilas+1,"Error en la función" + str(funcion))
			row = CSR_MAJOMA.fetchone()
			contFilas += 1
			barraProgreso.update(1)
			barraProgreso.refresh()
		#print("Filas migradas: " + str(contFilas))
		CSR_MAJOMA.close()
		del(CSR_MAJOMA)
	barraProgreso.close()

def migrarVisitas(prueba=False):
	sacarInfoArchivoMapeo()
	migrarObservacion("tb1RegistroAdmision",procesarFilatb1RegistroAdmision,prueba)

def main():
	aMigrar = [("tb2InicioAtencionMotivo",procesarFilatb2InicioAtencionMotivo),("tb3AccidenteViolenciaIntoxicacionEnvenenamientoQuemadura",procesarFilatb3AccidenteViolenciaIntoxicacionEnvenenamientoQuemadura),("tb4EmergenciaAntecedentesFamiliaresPersonales",procesarFilatb4EmergenciaAntecedentesFamiliaresPersonales),("tb5EmergenciaEnfermedadActual",procesarFilatb5EmergenciaEnfermedadActual)]
	sacarInfoArchivoMapeo()
	for i in aMigrar:
		migrarObservacion(i[0],i[1])

def mainPrueba():
	print("Comenzando migración de Tb1 hasta Tb5")
	aMigrar = [("tb2InicioAtencionMotivo",procesarFilatb2InicioAtencionMotivo),("tb3AccidenteViolenciaIntoxicacionEnvenenamientoQuemadura",procesarFilatb3AccidenteViolenciaIntoxicacionEnvenenamientoQuemadura),("tb4EmergenciaAntecedentesFamiliaresPersonales",procesarFilatb4EmergenciaAntecedentesFamiliaresPersonales),("tb5EmergenciaEnfermedadActual",procesarFilatb5EmergenciaEnfermedadActual)]
	sacarInfoArchivoMapeo()
	for i in aMigrar:
		migrarObservacion(i[0],i[1],prueba=True)
