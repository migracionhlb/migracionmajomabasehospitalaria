import sqlite3


def main():
	print("Creando base SQLite")

	baseSqlLite = "baseIDs"

	conn = sqlite3.connect(baseSqlLite)
	csr = conn.cursor()

	print("Creando Tabla idMedico_idProvider")
	query = """DROP TABLE IF EXISTS idMedico_idProvider;"""
	csr.execute(query)

	query = """CREATE TABLE idMedico_idProvider(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	idMedico integer not null,
	idProvider integer not null);"""
	csr.execute(query)

	print("Creando Tabla idPaciente_idPatient")
	query = """DROP TABLE IF EXISTS idPaciente_idPatient;"""
	csr.execute(query)

	query = """CREATE TABLE idPaciente_idPatient(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	idPaciente integer not null,
	idPatient integer not null);"""
	csr.execute(query)

	print("Creando tabla codigoIngreso_idVisit")
	query = """DROP TABLE IF EXISTS codigoIngreso_idVisit;"""
	csr.execute(query)

	query = """CREATE TABLE codigoIngreso_idVisit(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	codigoIngreso integer not null,
	idVisit integer not null);"""
	csr.execute(query)

	conn.commit()
	conn.close()

	print("Creación baseSqlLite exitosa.")

#Código para probar
def insertarEnidPaciente_idPatient(idPaciente,idPatient):
	conn = sqlite3.connect("baseIDs")
	csr = conn.cursor()
	query = "insert into idPaciente_idPatient(idPaciente,idPatient) values(" + str(idPaciente) + "," + str(idPatient) + ");"
	csr.execute(query)
	conn.commit()
	conn.close()

def idPatientPorIdPaciente(idPaciente):
	query = "SELECT * FROM idPaciente_idPatient where idPaciente = " + str(idPaciente)+ ";"
	cnx = sqlite3.connect("baseIDs")
	csr = cnx.cursor()
	csr.execute(query)
	print("Ejecuta query idPatientPorIdPaciente")
	retorno = csr.fetchone()
	print(retorno)
	if retorno is not None:
		return retorno[2]
	else:
		return 0
	
def probar():
	main()
	insertarEnidPaciente_idPatient(10,982312)
	print(idPatientPorIdPaciente(10))
	print(idPatientPorIdPaciente(1))
