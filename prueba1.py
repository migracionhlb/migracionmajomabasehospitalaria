import conexion
import consultas
from tqdm import tqdm

tablaAMigrar = "tb1RegistroAdmision"
contFilas = 0
cantFilasTablaAMigrar = consultas.numeroColumnas(tablaAMigrar)
contadorSinId = 0
listaSinId = []
lenBuffer = 100

barraProgreso = tqdm(range(cantFilasTablaAMigrar))
while contFilas < cantFilasTablaAMigrar:
	#print("Entra al while")
	query = "select * from (select *,ROW_NUMBER() over (order by(select NULL) desc) as numfila from " + tablaAMigrar + ")t where numfila > " + str(contFilas) + " and numfila < " + str(contFilas + lenBuffer + 1)
	CSR_MAJOMA = conexion.getConexionMCH().cursor()
	CSR_MAJOMA.execute(query)
	row = CSR_MAJOMA.fetchone()
	while row:
		idPatient = consultas.idPatientPorIdPaciente(int(row[3]))
		#print(row)
		if idPatient==0 and int(row[3]) not in listaSinId:
			contadorSinId += 1
			listaSinId.append(row[3])
		contFilas += 1
		barraProgreso.update(1)
		barraProgreso.refresh()
		row = CSR_MAJOMA.fetchone()
	CSR_MAJOMA.close()
barraProgreso.close()

print(listaSinId)
print(contadorSinId)

print(consultas.nombrePersonaExiste("'ALAN'","'ISAAC'","'SAAVEDRA'","'LOPEZ'"))
print(consultas.id_persona("'ALAN'","'ISAAC'","'SAAVEDRA'","'LOPEZ'"))
print(consultas.idPatientPorIdPaciente(63218))

