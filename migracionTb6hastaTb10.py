import conexion
import consultas
import uuid
import time
from migracionVisits import migrarEncounterObs
from tqdm import tqdm
import log

rutaArchivoMapeo = "./Mapeos/columnasAConceptos.csv"
lenBuffer = 100
#COMENTARIO PARA QUE VUELVA AL REPO EN LA MV
diccionarioMapeo = dict()

def sacarInfoArchivoMapeo():
	archivo = open(rutaArchivoMapeo,encoding="latin-1")
	archivo.readline()
	linea = archivo.readline()
	lista = []
	while linea:
		#print(linea)
		sep = linea.strip("\n").split(";")
		if sep[0] not in diccionarioMapeo:
			diccionarioMapeo[sep[0]] = dict()
		diccionarioMapeo[sep[0]][sep[2]] = dict()
		diccionarioMapeo[sep[0]][sep[2]]["posicion"] = int(sep[1])
		diccionarioMapeo[sep[0]][sep[2]]["tipoDato"] = sep[3]
		diccionarioMapeo[sep[0]][sep[2]]["observacion"] = sep[4]
		linea = archivo.readline()
	archivo.close()

def procesarFilatb6EmergenciaSignosVitales(fila,tablaAMigrar,contFilas):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		#Verificar fecha
		fecha = str(fila[5])
		prefijo = "tb6"

		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contFila):
			return False

		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contFilas,e)
		return False

def procesarFilatb7EmergenciaExamenFisicoDiagnostico(fila,tablaAMigrar,contFilas):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		#Verificar fecha
		fecha = str(fila[5])
		prefijo = "tb7"
		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contFilas):
			return False

		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contFilas,e)
		return False


def procesarFilatb8EmergenciaLocalizacionLesiones(fila,tablaAMigrar,contFilas):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		#Verificar fecha
		fecha = str(fila[5])
		prefijo = "tb8"

		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contFilas):
			return False

		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contFilas,e)
		return False

def procesarFilatb9EmergenciaObstetrica(fila,tablaAMigrar,contFilas):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		#Verificar fecha
		fecha = str(fila[5])
		prefijo = "tb9"

		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contFilas):
			return False

		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contFilas,e)
		return False

def procesarFilatb10SolicitudExamen(fila,tablaAMigrar,contFilas):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		fecha = str(fila[5])
		prefijo = "tb10"

		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,tablaAMigrar,prefijo,contFilas):
			return False

		return True
	except Exception as e:
		log.adding_log(tablaAMigrar,"Obs-Encounter",contFilas,e)
		return False

def migrarObservacion(tablaAMigrar,funcion,prueba=False):
	if not prueba:
		cantFilasTablaAMigrar = consultas.numeroColumnas(tablaAMigrar)
	else:
		cantFilasTablaAMigrar = 1000
	
	print("Migrando tabla", tablaAMigrar)

	contFilas = 0
	barraProgreso = tqdm(range(cantFilasTablaAMigrar))

	while contFilas < cantFilasTablaAMigrar:
		#print("Entra al while")
		query = "select * from (select *,ROW_NUMBER() over (order by(select NULL) desc) as numfila from " + tablaAMigrar + ")t where numfila > " + str(contFilas) + " and numfila < " + str(contFilas + lenBuffer + 1)
		CSR_MAJOMA = conexion.getConexionMCH().cursor()
		CSR_MAJOMA.execute(query)
		row = CSR_MAJOMA.fetchone()
		while row:
			if not funcion(row,tablaAMigrar,contFilas+1):
				log.adding_log(tablaAMigrar,"migrarObservacion",contFilas+1,"Error en la función" + str(funcion))
			row = CSR_MAJOMA.fetchone()
			contFilas += 1
			barraProgreso.update(1)
			barraProgreso.refresh()
		#print("Filas migradas: " + str(contFilas))
		CSR_MAJOMA.close()
		del(CSR_MAJOMA)
	barraProgreso.close()

def main():
	aMigrar = [("tb6EmergenciaSignosVitales",procesarFilatb6EmergenciaSignosVitales),("tb7EmergenciaExamenFisicoDiagnostico",procesarFilatb7EmergenciaExamenFisicoDiagnostico),("tb8EmergenciaLocalizacionLesiones",procesarFilatb8EmergenciaLocalizacionLesiones),("tb9EmergenciaObstetrica",procesarFilatb9EmergenciaObstetrica),("tb10SolicitudExamen",procesarFilatb10SolicitudExamen)]
	sacarInfoArchivoMapeo()
	for i in aMigrar:
		migrarObservacion(i[0],i[1])

def mainPrueba():
	print("Comenzando migración de Tb6 hasta Tb10")
	aMigrar = [("tb6EmergenciaSignosVitales",procesarFilatb6EmergenciaSignosVitales),("tb7EmergenciaExamenFisicoDiagnostico",procesarFilatb7EmergenciaExamenFisicoDiagnostico),("tb8EmergenciaLocalizacionLesiones",procesarFilatb8EmergenciaLocalizacionLesiones),("tb9EmergenciaObstetrica",procesarFilatb9EmergenciaObstetrica),("tb10SolicitudExamen",procesarFilatb10SolicitudExamen)]
	sacarInfoArchivoMapeo()
	#print(diccionarioMapeo)
	for i in aMigrar:
		migrarObservacion(i[0],i[1],prueba=True)

#