# -*- coding: utf-8 -*-
import conexion
import consultas
import uuid
import datetime
import log
from tqdm import tqdm

lenBuffer=10
#Conexion OpenMRS
#CNX_OPENMRS = conexion.conexion_openmrs()
#CSR_OPENMRS = CNX_OPENMRS.cursor()

diccionarioPerfiles = dict()
diccionarioPerfiles[1] = "'Organizational: System Administrator'"
diccionarioPerfiles[6] = "'Organizational: Doctor'"
diccionarioPerfiles[2] = "'Authenticated'"
diccionarioPerfiles[4] = "'Anonymous'"

# La idea es que retorne True si todo salio bien, y False si hubo un error.
def procesar_fila(fila,tabla_origen,contador):
    # Como va a ser lo primero en migrarse, por cada user se crea una persona.
    # Pueden haber personas sin users, pero no pueden haber users sin personas.
    try:
        nombre_persona = {}
        if " " in fila[1]:
            nombre_persona["PN"] = "'" + fila[1].split(" ")[0] + "'"
            nombre_persona["SN"] = "'" + fila[1].split(" ")[1] + "'"
        else:
            nombre_persona["PN"] = "'" + fila[1].split(" ")[0] + "'"
        if " " in fila[2]:
            nombre_persona["PA"] = "'" + fila[2].split(" ")[0] + "'"
            nombre_persona["SA"] = "'" + fila[2].split(" ")[1] + "'"
        else:
            nombre_persona["PA"] = "'" + fila[2].split(" ")[0] + "'"
        #Inserto persona del usuario en tabla person
        diccPerson = consultas.obtener_formato_tabla_openMRS("person")
        
        diccPerson["uuid"] = "'" + str(uuid.uuid1()) + "'"
        diccPerson["date_created"] = "'" + str(fila[7]) + "'"
        id_insercion_person = consultas.insertarEnTabla("person", diccPerson,tabla_origen,contador)
        if id_insercion_person == 0:
            return False
        diccPerson_Name = consultas.obtener_formato_tabla_openMRS("person_name")

        diccPerson_Name["uuid"] = "'" + str(uuid.uuid1()) + "'"
        diccPerson_Name["date_created"] = "'" + str(fila[7]) + "'"
        diccPerson_Name["person_id"] = id_insercion_person
        diccPerson_Name["preferred"] = 1
        diccPerson_Name["given_name"] = nombre_persona.get("PN","NULL")
        diccPerson_Name["middle_name"] = nombre_persona.get("SN","NULL")
        diccPerson_Name["family_name"] = nombre_persona.get("PA","NULL")
        diccPerson_Name["family_name2"] = nombre_persona.get("SA","NULL")
        diccPerson_Name["creator"] = 1
        id_insercion_pname = consultas.insertarEnTabla("person_name", diccPerson_Name,tabla_origen,contador)
        if id_insercion_pname == 0:
            return False
        diccionarioUsers = consultas.obtener_formato_tabla_openMRS("users")
        diccionarioUsers["username"] = "'" + fila[3] + "'"
        diccionarioUsers["system_id"] = "'" + "admin" + "'"
        diccionarioUsers["password"] = "'" + fila[4] + "'"
        diccionarioUsers["uuid"] = "'" + str(uuid.uuid1()) + "'"
        diccionarioUsers["date_created"] = "'" + str(fila[7]) + "'"
        diccionarioUsers["person_id"] = id_insercion_person
        diccionarioUsers["creator"] = 1
        id_insercion_user = consultas.insertarEnTabla("users", diccionarioUsers,tabla_origen,contador)
        if id_insercion_user == 0:
            return False

        perfil = diccionarioPerfiles[fila[5]]
        diccionarioUserRole = consultas.obtener_formato_tabla_openMRS("user_role")
        diccionarioUserRole["role"] = perfil
        diccionarioUserRole["user_id"] = id_insercion_user
        id_insercion_user_role = consultas.insertarEnTabla("user_role", diccionarioUserRole,tabla_origen,contador)
        if id_insercion_user_role == 0:
            return False
        return True
    except Exception as e:
        return False

def main():
    print("\nMIGRACIÓN USERS")
    ADD_USER_OPENMRS = "INSERT INTO users (username, password, person_id, uuid, system_id, retired, creator," \
                     "date_created, changed_by, date_changed) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"


    tablaAMigrar = "SEG_USUARIO"
    cantFilasTablaAMigrar = consultas.numeroColumnasSecurity(tablaAMigrar)
    contFilas = 0
    barraProgreso = tqdm(range(cantFilasTablaAMigrar))
    while contFilas < cantFilasTablaAMigrar:
        query = "select * from (select *,ROW_NUMBER() over (order by(select NULL) desc) as numfila from " + tablaAMigrar + ")t where numfila > " + str(contFilas) + " and numfila < " + str(contFilas + lenBuffer + 1)
        CSR_MAJOMA = conexion.getConexionMS().cursor()
        CSR_MAJOMA.execute(query)
        row = CSR_MAJOMA.fetchone()
        while row:
            try:
                procesar_fila(row,tablaAMigrar,contFilas+1)
            except Exception as e:
                log.adding_log(tablaAMigrar,"person",contFilas+1,e)
            row = CSR_MAJOMA.fetchone()
            contFilas +=1
            barraProgreso.update(1)
            barraProgreso.refresh()
        CSR_MAJOMA.close()