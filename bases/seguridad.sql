USE [MajomaSecurity]
GO
/****** Object:  Table [dbo].[SEG_TIPO_USUARIO]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEG_TIPO_USUARIO](
	[codigo] [int] NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_SEG_TIPO_USUARIO] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SEG_STATUS]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEG_STATUS](
	[codigo] [int] NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SEG_STATUS] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SEG_PERFIL]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEG_PERFIL](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[abreviatura] [nvarchar](3) NOT NULL,
	[usuario_ingreso] [int] NOT NULL,
	[fecha_ingreso] [smalldatetime] NOT NULL,
	[usuario_modificacion] [int] NOT NULL,
	[fecha_modificacion] [smalldatetime] NOT NULL,
	[pcname] [nvarchar](50) NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_SEG_PERFIL] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SG_OPCION_APLICACION]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SG_OPCION_APLICACION](
	[empresa] [int] NOT NULL,
	[sucursal] [int] NOT NULL,
	[modulo] [int] NOT NULL,
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](100) NOT NULL,
	[imagen] [nvarchar](100) NULL,
	[tipo] [int] NOT NULL,
	[ejecutable] [nvarchar](100) NULL,
	[usuario_ingreso] [int] NOT NULL,
	[fecha_ingreso] [smalldatetime] NOT NULL,
	[usuario_modificacion] [int] NULL,
	[fecha_modificacion] [smalldatetime] NULL,
	[pcname] [nvarchar](50) NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_SG_OPCION_APLICACION] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SG_MODULO]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SG_MODULO](
	[empresa] [int] NOT NULL,
	[sucursal] [int] NOT NULL,
	[codigo] [int] NOT NULL,
	[descripcion] [nvarchar](100) NOT NULL,
	[abreviatura] [nvarchar](3) NOT NULL,
	[usuario_ingreso] [int] NOT NULL,
	[fecha_ingreso] [smalldatetime] NOT NULL,
	[usuario_modificacion] [int] NOT NULL,
	[fecha_modificacion] [smalldatetime] NOT NULL,
	[pcname] [nvarchar](50) NULL,
	[status] [int] NOT NULL,
	[orden] [int] NULL,
 CONSTRAINT [PK_SG_MODULO] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbSecuencia]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSecuencia](
	[id] [int] NOT NULL,
	[descripcion] [nvarchar](100) NOT NULL,
	[secuencia] [int] NOT NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_tbSecuencia] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbProfesionUsuario]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbProfesionUsuario](
	[codigo] [int] NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[abreviatura] [nvarchar](10) NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_tbProfesionUsuario] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SG_PERFIL_USUARIO]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SG_PERFIL_USUARIO](
	[empresa] [int] NOT NULL,
	[sucursal] [int] NOT NULL,
	[codigo] [int] NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[usuario_ingreso] [int] NOT NULL,
	[fecha_ingreso] [smalldatetime] NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_SG_PERFIL_USUARIO] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SG_OPCION_APLICACION_POR_PERFIL]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SG_OPCION_APLICACION_POR_PERFIL](
	[empresa] [int] NOT NULL,
	[sucursal] [int] NOT NULL,
	[perfil] [int] NOT NULL,
	[modulo] [int] NOT NULL,
	[opcion_aplicacion] [int] NOT NULL,
	[superior] [int] NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_SG_OPCION_APLICACION_POR_PERFIL] PRIMARY KEY CLUSTERED 
(
	[perfil] ASC,
	[modulo] ASC,
	[opcion_aplicacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SEG_USUARIO]    Script Date: 12/13/2018 15:27:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEG_USUARIO](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
	[apellido] [nvarchar](50) NOT NULL,
	[usuario] [nvarchar](15) NOT NULL,
	[password] [nvarchar](15) NOT NULL,
	[perfil] [int] NOT NULL,
	[profesion] [int] NULL,
	[abreviatura] [nvarchar](10) NULL,
	[usr_ingreso] [int] NOT NULL,
	[fec_ingreso] [smalldatetime] NOT NULL,
	[usr_modificacion] [int] NOT NULL,
	[fec_modificacion] [smalldatetime] NOT NULL,
	[pc_name] [nvarchar](40) NOT NULL,
	[status] [int] NOT NULL,
	[PermitirAnular] [int] NULL,
 CONSTRAINT [PK_SEG_USUARIO] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_SEG_USUARIO_SEG_PERFIL]    Script Date: 12/13/2018 15:27:50 ******/
ALTER TABLE [dbo].[SEG_USUARIO]  WITH CHECK ADD  CONSTRAINT [FK_SEG_USUARIO_SEG_PERFIL] FOREIGN KEY([perfil])
REFERENCES [dbo].[SEG_PERFIL] ([codigo])
GO
ALTER TABLE [dbo].[SEG_USUARIO] CHECK CONSTRAINT [FK_SEG_USUARIO_SEG_PERFIL]
GO
/****** Object:  ForeignKey [FK_SG_OPCION_APLICACION_POR_PERFIL_SEG_PERFIL]    Script Date: 12/13/2018 15:27:50 ******/
ALTER TABLE [dbo].[SG_OPCION_APLICACION_POR_PERFIL]  WITH CHECK ADD  CONSTRAINT [FK_SG_OPCION_APLICACION_POR_PERFIL_SEG_PERFIL] FOREIGN KEY([perfil])
REFERENCES [dbo].[SEG_PERFIL] ([codigo])
GO
ALTER TABLE [dbo].[SG_OPCION_APLICACION_POR_PERFIL] CHECK CONSTRAINT [FK_SG_OPCION_APLICACION_POR_PERFIL_SEG_PERFIL]
GO
/****** Object:  ForeignKey [FK_SG_OPCION_APLICACION_POR_PERFIL_SG_OPCION_APLICACION1]    Script Date: 12/13/2018 15:27:50 ******/
ALTER TABLE [dbo].[SG_OPCION_APLICACION_POR_PERFIL]  WITH CHECK ADD  CONSTRAINT [FK_SG_OPCION_APLICACION_POR_PERFIL_SG_OPCION_APLICACION1] FOREIGN KEY([modulo])
REFERENCES [dbo].[SG_OPCION_APLICACION] ([codigo])
GO
ALTER TABLE [dbo].[SG_OPCION_APLICACION_POR_PERFIL] CHECK CONSTRAINT [FK_SG_OPCION_APLICACION_POR_PERFIL_SG_OPCION_APLICACION1]
GO
