# -*- coding: utf-8 -*-

import pyodbc
import mysql.connector as MySqlConnector  # Se necesita instalar el driver de MySql para python
import consultas
import sqlite3
import log

def conexion_majoma_security():
	###Conexión MAJOMA
	host="192.168.1.200"
	server = "192.168.1.200"  # Nombre del servidor donde se encuentra la base de datos
	database = "GestionDSecurity"  # Nombre de la base de datos a utilizar
	## Esto se debe colocar como propiedades en el string de la conexión para
	username = 'pasantias'
	password = 'Hospital2019'
	# En la cnxn se especifica el driver de la versión de SQL Server el cual ya debe estar instalado.	
	cnx_majoma = pyodbc.connect('Trusted_Connection=no',driver='{ODBC Driver 13 for SQL Server}',server=server,host=host,database=database,uid=username,pwd=password)
	return cnx_majoma

def conexion_majoma_controlhospitalario():
	###Conexión MAJOMA
	host="192.168.1.200"
	server = "192.168.1.200"   # Nombre del servidor donde se encuentra la base de datos
	database = 'MajomaControlHospitalario'  # Nombre de la base de datos a utilizar
	## Esto se debe colocar como propiedades en el string de la conexión para
	username = 'pasantias'
	password = 'Hospital2019'
	# En la cnxn se especifica el driver de la versión de SQL Server el cual ya debe estar instalado.
	cnx_majoma = pyodbc.connect('Trusted_Connection=no',driver='{ODBC Driver 13 for SQL Server}',server=server,host=host,database=database,uid=username,pwd=password)
	cnx_majoma.setencoding('latin1')
	return cnx_majoma

def conexion_openmrs():
	cnx_openmrs = MySqlConnector.connect(user="root",password="hospital",host="127.0.0.1",database="openmrs")	
	cnx_openmrs.set_charset_collation('latin1')
	return cnx_openmrs

def conexionSqlite():
	conn = sqlite3.connect("baseIDs")
	return conn

conexionMS = conexion_majoma_security()
conexionMCH = conexion_majoma_controlhospitalario()
conexionOMRS = conexion_openmrs()
conexionSqlite = sqlite3.connect("baseIDs")

def main():
	conexionMS = conexion_majoma_security()
	conexionMCH = conexion_majoma_controlhospitalario()
	conexionOMRS = conexion_openmrs()
	conexionSqlite = sqlite3.connect("baseIDs")
	print("Corrió el main de conexion")

def cerrarConexiones():
	conexionMS.close()
	conexionMCH.close()
	conexionOMRS.close()
	conexionSqlite.close()


def getConexionMS():
	return conexionMS

def getConexionMCH():
	return conexionMCH

def getConexionOMRS():
	return conexionOMRS

def getConexionSqlite():
	return conexionSqlite
