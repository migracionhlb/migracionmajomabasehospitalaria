import time
import os

def directorio_log():
        dirName='log'
        if not os.path.exists(dirName):
                os.mkdir(dirName)

def adding_log(tabla_origen,tabla_destino,numero_fila,exception):        
        filename = 'log/log'+time.strftime("%b%d%Y")
        if os.path.exists(filename):
                append_write = 'a' # append if already exists
        else:
                append_write = 'w' # make a new file if not
        log = open(filename,append_write)
        log.write(time.strftime("%b:%d:%Y-%H:%M:%S") + ',Tabla origen:'+str(tabla_origen)+',Tabla destino:'+str(tabla_destino)+',Fila:'+str(numero_fila) +',Error:'+ str(exception) + '\n')
        log.close()

        
