import consultas
import uuid
import time
import log
#Este Script debe ser corrido después de la migración de Users

#Tipos de atributos de personas
def main(): 
	nombreTabla = "person_attribute_type"
	nombreArchivo = "atributosPersonas.csv"
	print("Comenzando migración de tabla " + nombreTabla + "...")
	archivo = open(nombreArchivo)
	archivo.readline() #Encabezado
	linea = archivo.readline()
	cont = 0
	while linea:
		tabla = consultas.obtener_formato_tabla_openMRS(nombreTabla)
		tabla["name"] = "'" + linea.split(";")[0].upper() + "'"
		tabla["description"] = "'" + linea.split(";")[1] + "'"
		tabla["searchable"] = 1
		tabla["creator"] = 1
		tabla["uuid"] = "'" + str(uuid.uuid1()) + "'"
		tabla["retired"] = 0
		tabla["date_created"] = "'" + time.strftime('%Y-%m-%d %H:%M:%S') + "'"
		consultas.insertarEnTabla(nombreTabla,tabla,"CSV Atributo de personas",cont+1)
		linea = archivo.readline()
		cont += 1
	print("Migración de tabla " + nombreTabla + " terminada...")
	archivo.close()

	#Tipos de atributos de providers
	nombreTabla = "provider_attribute_type"
	nombreArchivo = "atributosProvider.csv"
	print("Comenzando migración de tabla " + nombreTabla + "...")
	archivo = open(nombreArchivo)
	archivo.readline() #Encabezado
	linea = archivo.readline()
	cont = 0
	while linea:
		tabla = consultas.obtener_formato_tabla_openMRS(nombreTabla)
		tabla["name"] = "'" + linea.split(";")[0].upper() + "'"
		tabla["description"] = "'" + linea.split(";")[1] + "'"
		tabla["min_occurs"] = 0
		tabla["creator"] = 1
		tabla["uuid"] = "'" + str(uuid.uuid1()) + "'"
		tabla["retired"] = 0
		tabla["date_created"] = "'" + time.strftime('%Y-%m-%d %H:%M:%S') + "'"
		try:
			consultas.insertarEnTabla(nombreTabla,tabla,"CSV Atributo de proveedor",cont+1)
		except Exception as e:
			log.adding_log("CSV Atributo de proveedor",nombreTabla,cont+1,e)
		linea = archivo.readline()
		cont += 1
	print("Migración de tabla " + nombreTabla + " terminada...")
	archivo.close()
