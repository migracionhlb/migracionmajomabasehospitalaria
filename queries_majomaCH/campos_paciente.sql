USE MajomaControlHospitalario;

-- Consulta de atributos de tbPaciente

-- Cedula de paciente, nombres, appellidos y genero
GO
SELECT p.cedula, p.primer_nombre, p.segundo_nombre, p.apellido_paterno, p.apellido_materno, p.genero
FROM tbPaciente p
GO

-- Tipo de identificacion de paciente
GO
SELECT tip.abreviatura, tip.descripcion
FROM tbPaciente p
INNER JOIN tbTipoIdentificacionPersona tip ON p.id = tip.codigo
GO

-- Fecha de nacimiento del paciente
GO
SELECT p.fecha_nacimiento
FROM tbPaciente p
GO

-- Lugar de nacimiento y ocupacion del paciente
GO
SELECT p.lugar_nacimiento, p.ocupacion
FROM tbPaciente p
GO

-- Estado civil del paciente
GO
SELECT ec.descripcion
FROM tbPaciente p
INNER JOIN tbEstadoCivil ec ON p.estado_civil = ec.codigo
GO

-- Tipo de sangre del paciente
GO
SELECT ts.descripcion
FROM tbPaciente p
INNER JOIN tbTipoSangre ts ON p.tipo_sangre = ts.codigo
GO

-- Nacionalidad del paciente
GO
SELECT pais.descripcion, pais.abreviatura
FROM tbPaciente p
INNER JOIN tbPais pais ON p.nacionalidad = pais.codigo
GO

-- Pais del paciente
GO
SELECT pais.abreviatura, pais.descripcion
FROM tbPaciente p
INNER JOIN tbPais pais ON p.pais = pais.codigo
GO

-- Provincia del paciente
GO
SELECT prov.abreviatura, prov.descripcion, prov.cliprovincia, pais.abreviatura AS pais
FROM tbPaciente p
INNER JOIN tbProvincia prov ON p.provincia = prov.codigo
INNER JOIN tbPais pais ON prov.pais = pais.codigo
GO

-- Ciudad del paciente
GO
SELECT ciud.abreviatura, ciud.descripcion, prov.abreviatura AS provincia, pais.abreviatura AS pais
FROM tbPaciente p
INNER JOIN tbCiudad ciud ON p.ciudad = ciud.codigo
INNER JOIN tbProvincia prov ON ciud.provincia = prov.codigo
INNER JOIN tbPais pais ON prov.pais = pais.codigo
GO

-- Paroquia del paciente
GO
SELECT parr.descripcion, ciud.abreviatura AS ciudad, prov.abreviatura AS provincia, pais.abreviatura AS pais
FROM tbPaciente p
INNER JOIN tbParroquia parr ON p.parroquia = parr.codigo
INNER JOIN tbCiudad ciud ON parr.ciudad = ciud.codigo
INNER JOIN tbProvincia prov ON ciud.provincia = prov.codigo
INNER JOIN tbPais pais ON prov.pais = pais.codigo
GO

-- Direccion, telefono, celular del paciente
GO
SELECT p.direccion, p.telefono, p.celular, p.otro, p.observacion
FROM tbPaciente p
GO

-- Titular del paciente, tipo de beneficiario, tipo de parentesco y lugar de trabajo
GO
SELECT p.cedula_titular, beneficiario.descripcion AS beneficiario, parentesco.descripcion AS parentesco, p.lugar_trabajo
FROM tbPaciente p
INNER JOIN tbTipoBeneficiario beneficiario ON p.tipo_beneficiario = beneficiario.codigo
INNER JOIN tbParentesco parentesco ON p.tipo_parentesco = parentesco.codigo
GO

-- Tipo de seguro
GO
SELECT ts.abrebiatura, ts.descripcion, ts.dependencia
FROM tbPaciente p
INNER JOIN tbTipoSeguro ts ON p.tipo_seguro = ts.codigo
GO

-- Des campos de paciente
GO
SELECT p.des_campo1, p.des_campo2, p.des_campo3
FROM tbPaciente p
GO

-- Usuario de ingreso del paciente
GO
SELECT u.usuario, u.password, u.nombre, u.apellido, per.abreviatura AS perfil_abreviatura, per.descripcion AS perfil_descripcion
FROM tbPaciente p
INNER JOIN GestionDSecurity..SEG_USUARIO u ON p.usuario_ingreso = u.codigo
INNER JOIN GestionDSecurity..SEG_PERFIL per ON u.perfil = per.codigo
GO

-- Fecha de ingreso del paciente
GO
SELECT p.fecha_ingreso
FROM tbPaciente p
GO

-- Usuario de modificacion del paciente
GO
SELECT u.usuario, u.password, u.nombre, u.apellido, per.abreviatura AS perfil_abreviatura, per.descripcion AS perfil_descripcion
FROM tbPaciente p
INNER JOIN GestionDSecurity..SEG_USUARIO u ON p.usuario_modificacion = u.codigo
INNER JOIN GestionDSecurity..SEG_PERFIL per ON u.perfil = per.codigo
GO

--Fecha de modificacion del paciente
GO
SELECT p.fecha_modificacion
FROM tbPaciente p
GO

-- Otros campos de paciente
GO
SELECT p.pcname, p.status, p.status_discapacidad, p.carnet_conadis, p.status_otro_seguro, p.tipo_seguro_iess, p.descripcion_otro_seguro
FROM tbPaciente p
GO