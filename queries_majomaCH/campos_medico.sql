USE MajomaControlHospitalario;

-- Consulta de atributos de tbMedico

-- Cedula de medico, nombres, appellidos, sexo y direcion
GO
SELECT m.cedula, m.nombres, m.apellidos, m.sexo, m.direccion
FROM tbMedico m
GO

-- Registro profesinal del medico, telefono
GO
SELECT m.registro_profesional, m.telefono
FROM tbMedico m
GO

-- Especializacion del medico
GO
SELECT esp.descripcion
FROM tbMedico m
INNER JOIN tbEspecializacion esp ON m.especializacion = esp.codigo
GO

-- Tipo de medico
GO
SELECT tm.abreviatura, tm.descripcion
FROM tbMedico m
INNER JOIN tbTipoMedico tm ON m.tipo_medico = tm.codigo
GO

-- Correo electronico, des_campo2 y des_campo3 de medico
GO
SELECT m.des_campo1, m.des_campo2, m.des_campo3
FROM tbMedico m
GO

-- Fecha de ingreso del medico
GO
SELECT m.fecha_registro
FROM tbMedico m
GO

-- Usuario de ingreso del medico
GO
SELECT u.usuario, u.password, u.nombre, u.apellido, per.abreviatura AS perfil_abreviatura, per.descripcion AS perfil_descripcion
FROM tbMedico m
INNER JOIN GestionDSecurity..SEG_USUARIO u ON m.usuario_registro = u.codigo
INNER JOIN GestionDSecurity..SEG_PERFIL per ON u.perfil = per.codigo
GO

--Fecha de modificacion del medico
GO
SELECT m.fecha_modificacion
FROM tbMedico m
GO

-- Usuario de modificacion del medico
GO
SELECT u.usuario, u.password, u.nombre, u.apellido, per.abreviatura AS perfil_abreviatura, per.descripcion AS perfil_descripcion
FROM tbMedico m
INNER JOIN GestionDSecurity..SEG_USUARIO u ON m.usuario_modificacion = u.codigo
INNER JOIN GestionDSecurity..SEG_PERFIL per ON u.perfil = per.codigo
GO

-- Servicios medicos del medico
GO
SELECT sm.id_inec, sm.descripcion, sm.status
FROM tbMedico m
INNER JOIN tbServiciosMedicos sm ON m.servicios_medico = sm.id
GO

-- Tipo de servicio del medico
GO
SELECT ts.descripcion, ts.status
FROM tbMedico m
INNER JOIN tbTipoServicio ts ON m.tipo_medico_servicio = ts.codigo
GO

-- Otros campos de medico
GO
SELECT m.pcname, m.status, m.registro_sanitario, m.libro, m.tomo, m.folio
FROM tbMedico m
GO

-- Tipo de atencion que da el medico
GO
SELECT a.descripcion, a.status
FROM tbMedico m
INNER JOIN tbAtencion a ON m.numero_atencion = a.codigo
GO

-- Consultorio del medico
GO
SELECT m.consultorio_default
FROM tbMedico m
GO