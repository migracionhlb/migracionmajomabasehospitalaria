import creacionBaseSqlite
import migracionUsers
import migracionAtributosPersonas
import migracionConceptosTablasEnumeradas
import migracionPersonas
import migracionVisits
import conexion
import log


def migracionObligatoria():
	log.directorio_log()
	
	#Migraciones que deben hacerse en su totalidad
	creacionBaseSqlite.main()
	migracionAtributosPersonas.main()
	migracionConceptosTablasEnumeradas.main()
	migracionUsers.main()
	

def main():
	migracionObligatoria()

	#Migraciones que deben hacerse en limitado
	migracionPersonas.main()
	migracionVisits.migrarVisitas()
	migracionVisits.main()
	migracionTb6hastaTb10.main()
	migraciontb11Hastatb15.main()
	migraciontb16Hastatb20.main()

def mainPrueba():
	migracionObligatoria()
	migracionPersonas.main(prueba=True)
	migracionVisits.migrarVisitas(prueba=True)
	migracionVisits.mainPrueba()
	migracionTb6hastaTb10.mainPrueba()
	migraciontb11Hastatb15.mainPrueba()
	migraciontb16Hastatb20.mainPrueba()
	
menu = """Seleccione una opción:
1. Migración Total
2. Migración de prueba
3. Salir"""

ingreso = ""
while ingreso != "3":
	print(menu + "\n")
	ingreso = input("Escoja una opción: ")
	if ingreso == "1":
		main()
	elif ingreso == "2":
		mainPrueba()
	elif ingreso == "3":
		print("Fin del programa.")
	else:
		print("Ingrese una opción correcta")
