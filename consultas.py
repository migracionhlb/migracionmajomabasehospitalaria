import conexion
import sqlite3
import log

def obtener_formato_tabla_openMRS(tabla):
	cursor = conexion.getConexionOMRS().cursor()
	cursor.execute("describe "+tabla)
	diccionario = dict()
	if tabla != "user_role":
		cursor.fetchone()
	row = cursor.fetchone()  # Dos veces porque así no agarramos la primary key que siempre es autoincrement.
	while row:
		if row[2] == 'NO' and row[4] != "":
			diccionario[row[0]] = row[4]  # Si tiene un valor por default, se coloca el valor por Default
		else:
			diccionario[row[0]] = None
		row = cursor.fetchone()
	cursor.close()
	return diccionario

def insertarEnTabla(tabla, diccionario,tabla_origen,contador):
	try:
		query = "INSERT INTO " + tabla + " (" + ",".join(list(diccionario.keys()))+") values ("
		for item in diccionario.keys():
			if diccionario[item] is None:
				query += "NULL,"
			else:
				query += "{"+item+"},"
		query = query[:len(query)-1]+")"
		cursor = conexion.getConexionOMRS().cursor()
		cursor.execute(query.format(**diccionario))
		if tabla == "user_role":
			cursor.close()
			return 1
		id = cursor.lastrowid
		conexion.getConexionOMRS().commit()
		cursor.close()
		return id
	except Exception as e:
		log.adding_log(tabla_origen,tabla,contador,e)
		#print("Error en función insertar en tabla " + tabla)
		return 0

def nombreColumnaId(nombreTabla):
	query = "describe {}"
	csr = conexion.getConexionOMRS().cursor(buffered=True)
	csr.execute(query.format(nombreTabla))
	retorno = csr.fetchone()[0]
	csr.close()
	return retorno

def actualizarEnTabla(tabla,idCampo, diccionarioActualizado,tabla_origen,contador):
	try:
		query = "UPDATE " + tabla + " SET "
		for item in diccionarioActualizado.items():
			if item[1] is None:
				query += item[0] + " = NULL, "
			else:
				query += item[0] + " = {"+item[0]+"}, "
		query = query[:len(query)-2] + " WHERE " + nombreColumnaId(tabla) + " = " + str(idCampo)
		cursor = conexion.getConexionOMRS().cursor(buffered=True)
		query = query.format(**diccionarioActualizado)
		cursor.execute(query)
		conexion.getConexionOMRS().commit()
		cursor.close()
		del(cursor)
		return idCampo
	except Exception as e:
		log.adding_log(tabla_origen,tabla,contador,e)
		#print("Error en función actualizarEnTabla")
		return 0

def atributoEnTabla(tabla, nombreAtributo):
	try:
		query = "SELECT * FROM " + tabla + " WHERE name=" + nombreAtributo
		cursor = conexion.getConexionOMRS().cursor()
		cursor.execute(query)
		retorno = cursor.fetchone()
		cursor.close()
		if retorno is None:
			return False
		return True
	except:
		print("Error en función atributo en tabla")
		return False

def conceptoClaseEnTabla(tabla, nombreClase):
	try:		
		query = 'SELECT * FROM '  + tabla + ' WHERE name="' + nombreTipo +'"'
		cursor = conexion.getConexionOMRS().cursor()
		cursor.execute(query)
		retorno = cursor.fetchone()
		cursor.close()
		if retorno is None:
			return False		
		return True
	except:
		print("Error en función conceptoClaseEnTabla")
		return False

def conceptoTipoEnTabla(tabla, nombreTipo):
	try:
		query = 'SELECT * FROM '  + tabla + ' WHERE name="' + nombreTipo +'"'
		cursor = conexion.getConexionOMRS().cursor()
		cursor.execute(query)
		row=cursor.fetchone()
		cursor.close()
		return row[0]
	except Exception as e:
		print("Error en función conceptoTipoEnTabla")
		return False

def obtenerInfoPorId(tabla,idCampo):
	try:
		diccionario = obtener_formato_tabla_openMRS(tabla)
		query = "SELECT "
		lista = list(diccionario.keys())
		for key in lista:
			query += key + ", "
		query = query[:len(query)-2] + " FROM " + tabla + " WHERE " + tabla + "_id = " + str(idCampo)
		cursor = conexion.getConexionOMRS().cursor()
		cursor.execute(query)
		row = cursor.fetchone()
		for i in range(len(lista)):
			if row[i] is not None:
				try:
					int(float(row[i]))
					diccionario[lista[i]] = row[i]
				except:
					diccionario[lista[i]] = "'" + str(row[i]) + "'"
		cursor.close()
		return diccionario
	except:
		print("Error en función obtenerInfoPorId")
		return None

def insertarEnidMedico_idProvider(idMedico,idProvider):
	csr = conexion.getConexionSqlite().cursor()
	query = "insert into idMedico_idProvider(idMedico,idProvider) values(" + str(idMedico) + "," + str(idProvider) + ");"
	csr.execute(query)
	conexion.getConexionSqlite().commit()
	csr.close()

def insertarEnidPaciente_idPatient(idPaciente,idPatient):
	csr = conexion.getConexionSqlite().cursor()
	query = "insert into idPaciente_idPatient(idPaciente,idPatient) values(" + str(idPaciente) + "," + str(idPatient) + ");"
	csr.execute(query)
	conexion.getConexionSqlite().commit()
	csr.close()

def insertarEnCodigoIngreso_idVisit(codigoIngreso,idVisit):
	csr = conexion.getConexionSqlite().cursor()
	query = "insert into codigoIngreso_idVisit(codigoIngreso,idVisit) values(" + str(codigoIngreso) + "," + str(idVisit) + ");"
	csr.execute(query)
	conexion.getConexionSqlite().commit()
	csr.close()

def nombrePersonaExiste(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno):
	query = "SELECT person_id FROM person_name WHERE given_name={} and middle_name={} and family_name={} and family_name2={}"
	csr = conexion.getConexionOMRS().cursor()
	csr.execute(query.format(primer_nombre, segundo_nombre, apellido_paterno, apellido_materno))
	retorno = csr.fetchone()
	#print(retorno)
	csr.close()
	if retorno is None:
		return False
	else:
		return True

def id_persona(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno):
	query = "SELECT person_id FROM person_name WHERE given_name={} and middle_name={} and family_name={} and family_name2={}"
	csr = conexion.getConexionOMRS().cursor()
	csr.execute(query.format(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno))
	retorno = csr.fetchone()[0]
	#print(retorno)
	csr.close()
	return retorno

def tipo_atributo_persona_existe(nombre_atributo):
	query = "SELECT name FROM person_attribute_type WHERE name = {}"
	csr = conexion.getConexionOMRS().cursor()
	csr.execute(query.format(nombre_atributo))
	retorno = csr.fetchone()
	csr.close()
	return retorno  # Esto retorna True o False

def id_tipo_atributo_persona(nombre_atributo):
	query = "SELECT person_attribute_type_id FROM person_attribute_type where name = '{}'"
	csr = conexion.getConexionOMRS().cursor()
	query = query.format(nombre_atributo.strip("\n"))
	csr.execute(query)
	retorno = int(csr.fetchone()[0])
	csr.close()
	return retorno

def id_tipo_atributo_provider(nombre_atributo):
	query = "SELECT provider_attribute_type_id FROM provider_attribute_type where name = '{}'"
	csr = conexion.getConexionOMRS().cursor()
	csr.execute(query.format(nombre_atributo))
	retorno = csr.fetchone()[0]
	csr.close()
	return retorno

def numeroColumnas(nombreTabla):
	query = "SELECT COUNT(*) from " + nombreTabla
	csr = conexion.getConexionMCH().cursor()
	csr.execute(query.format(nombreTabla))
	retorno = csr.fetchone()[0]
	csr.close()
	return int(retorno)

def numeroColumnasSecurity(nombreTabla):
	query = "SELECT COUNT(*) from " + nombreTabla
	csr = conexion.getConexionMS().cursor()
	csr.execute(query.format(nombreTabla))
	retorno = csr.fetchone()[0]
	csr.close()
	return int(retorno)

def idPersonaEnPatients(idPersona):
	query = "SELECT patient_id FROM patient WHERE patient_id = " + str(idPersona)
	csr = conexion.getConexionOMRS().cursor()
	csr.execute(query)
	retorno = csr.fetchone()
	#print(retorno)
	csr.close()
	if retorno is None:
		return False
	else:
		return True

def existePacienteEnBaseHospital(idPaciente):
	query = "SELECT primer_nombre,segundo_nombre,apellido_paterno,apellido_materno FROM tbPaciente WHERE id = " + idPaciente
	csr = conexion.getConexionMCH().cursor()
	csr.execute(query)
	retorno = csr.fetchone()
	#print(retorno)
	csr.close()
	if retorno is None:
		return False
	else:
		return True

def nombresPacienteHospital(idPaciente):
	query = "SELECT primer_nombre,segundo_nombre,apellido_paterno,apellido_materno FROM tbPaciente WHERE id = " + idPaciente
	csr = conexion.getConexionMCH().cursor()
	csr.execute(query)
	retorno = csr.fetchone()
	#print(retorno)
	csr.close()
	return list(retorno)

def existeProviderEnBaseHospital(idProvider):
	query = "SELECT * FROM tbMedico WHERE id = " + idProvider
	csr = conexion.getConexionMCH().cursor()
	csr.execute(query)
	retorno = csr.fetchone()
	#print(retorno)
	csr.close()
	if retorno is None:
		return False
	else:
		return True

def nombreConceptoExiste(nombre):
	query = "SELECT * FROM concept_name WHERE name = " + "'" + nombre + "'"
	#print("f1 ",end="")
	csr = conexion.getConexionOMRS().cursor(buffered=True)
	csr.execute(query)
	retorno = csr.fetchone()
	csr.close()
	if retorno is None:
		return False
	else:
		return True

def idConceptoPorNombre(nombre):
	query = "SELECT concept_id FROM concept_name WHERE name = '" + nombre + "'"
	csr = conexion.getConexionOMRS().cursor(buffered=True)
	csr.execute(query)
	retorno = csr.fetchone()[0]
	#print(retorno)
	csr.close()
	return retorno

def idPatientPorIdPaciente(idPaciente):
	query = "SELECT idPatient FROM idPaciente_idPatient where idPaciente = " + str(idPaciente)+ ";"
	csr = conexion.getConexionSqlite().cursor()
	csr.execute(query)
	retorno = csr.fetchone()
	csr.close()
	if not retorno:
		return 0
	return retorno[0]

def idProviderPorIdMedico(idMedico):
	query = "SELECT * FROM idMedico_idProvider where idMedico = " + str(idMedico)+ ";"
	csr = conexion.getConexionSqlite().cursor()
	csr.execute(query)
	retorno = csr.fetchone()
	csr.close()
	if not retorno:
		return 0
	return retorno[0]

def idVisitPorCodigoIngreso(idCodigoIngreso):
	query = "SELECT * FROM codigoIngreso_idVisit where codigoIngreso = " + str(idCodigoIngreso)+ ";"
	csr = conexion.getConexionSqlite().cursor()
	csr.execute(query)
	retorno = csr.fetchone()
	csr.close()
	if not retorno:
		return 0
	return retorno[0]
