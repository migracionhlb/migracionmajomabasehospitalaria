import conexion
import consultas
import uuid
import time
from migracionVisits import migrarEncounterObs
from tqdm import tqdm
import log

rutaArchivoMapeo = "./Mapeos/columnasAConceptos.csv"
lenBuffer = 100
#COMENTARIO PARA QUE VUELVA AL REPO EN LA MV
diccionarioMapeo = dict()

def sacarInfoArchivoMapeo():
	archivo = open(rutaArchivoMapeo,encoding="latin-1")
	archivo.readline()
	linea = archivo.readline()
	lista = []
	while linea:
		#print(linea)
		sep = linea.strip("\n").split(";")
		if sep[0] not in diccionarioMapeo:
			diccionarioMapeo[sep[0]] = dict()
		diccionarioMapeo[sep[0]][sep[2]] = dict()
		diccionarioMapeo[sep[0]][sep[2]]["posicion"] = int(sep[1])
		diccionarioMapeo[sep[0]][sep[2]]["tipoDato"] = sep[3]
		diccionarioMapeo[sep[0]][sep[2]]["observacion"] = sep[4]
		linea = archivo.readline()
	archivo.close()
	
def procesarFilatb18OrdenLaboratorio(fila,nombreTabla,contFilas):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[2])
		idProvider = consultas.idProviderPorIdMedico(fila[4])
		fecha = str(fila[12])
		prefijo = "tb18"
		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,nombreTabla,prefijo,contFilas):
			return False

		return True
	except Exception as e:
		log.adding_log(nombreTabla,"Obs-Encounter",contFilas,e)
		return False


def procesarFila19PrescripcionInsumos(fila,nombreTabla,contFilas):
	try:
		idVisita = consultas.idVisitPorCodigoIngreso(fila[1])
		idPatient = consultas.idPatientPorIdPaciente(fila[3])
		idProvider = consultas.idProviderPorIdMedico(fila[5])
		fecha = str(fila[13])
		prefijo = "tb19"
		if not migrarEncounterObs(fila,idVisita,idPatient,idProvider,fecha,nombreTabla,prefijo,contFilas):
			return False

		return True
	except Exception as e:
		log.adding_log(nombreTabla,"Obs-Encounter",contFilas,e)
		return False
	
def migrarObservacion(tablaAMigrar,funcion,prueba=False):
	if not prueba:
		cantFilasTablaAMigrar = consultas.numeroColumnas(tablaAMigrar)
	else:
		cantFilasTablaAMigrar = 1000
	print("Migrando tabla", tablaAMigrar)
	
	contFilas = 0

	barraProgreso = tqdm(range(cantFilasTablaAMigrar))

	while contFilas < cantFilasTablaAMigrar:
		#print("Entra al while")
		query = "select * from (select *,ROW_NUMBER() over (order by(select NULL) desc) as numfila from " + tablaAMigrar + ")t where numfila > " + str(contFilas) + " and numfila < " + str(contFilas + lenBuffer + 1)
		CSR_MAJOMA = conexion.getConexionMCH().cursor()
		CSR_MAJOMA.execute(query)
		row = CSR_MAJOMA.fetchone()
		while row:
			if not funcion(row,tablaAMigrar,contFilas+1):
				log.adding_log(tablaAMigrar,"migrarObservacion",contFilas+1,"Error en la función" + str(funcion))
			row = CSR_MAJOMA.fetchone()
			contFilas += 1
			barraProgreso.update(1)
			barraProgreso.refresh()
		#print("Filas migradas: " + str(contFilas))
		CSR_MAJOMA.close()
		del(CSR_MAJOMA)
	barraProgreso.close()
        

def main():
	aMigrar = [("tb18OrdenLaboratorio",procesarFilatb18OrdenLaboratorio),("tb19PrescripcionInsumos",procesarFila19PrescripcionInsumos)]
	sacarInfoArchivoMapeo()
	for i in aMigrar:
		migrarObservacion(i[0],i[1])

def mainPrueba():
	print("Comenzando migración de Tb16 hasta Tb20")
	aMigrar = [("tb18OrdenLaboratorio",procesarFilatb18OrdenLaboratorio),("tb19PrescripcionInsumos",procesarFila19PrescripcionInsumos)]
	sacarInfoArchivoMapeo()
	for i in aMigrar:
		migrarObservacion(i[0],i[1],prueba=True)


      
