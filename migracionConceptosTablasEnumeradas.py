import conexion
import consultas
import uuid
import sqlite3
import time
import log
from tqdm import tqdm

def diccionarioTipoDeDato():
	tipoDeDato = dict()
	tipoDeDato["int"] = 1 # Id del datatype que tendrá el concepto en Openmrs
	tipoDeDato["nvarchar"] = 3
	tipoDeDato["bit"] = 1
	tipoDeDato["decimal"] = 1
	tipoDeDato["smalldatetime"] = 8
	tipoDeDato["nchar"] = 3
	return tipoDeDato

def prefijo(linea):
	if linea[3].isalpha():
		return linea[:3]
	else:
		return linea[:4]

def main():
	print("MIGRACIÓN CONCEPTO TABLAS ENUMERADAS")
	ruta = "./Mapeos/columnasAConceptos.csv"
	archivo = open(ruta,encoding="latin-1")
	archivo.readline()
	linea = archivo.readline()
	cont = 0
	barraProgreso = tqdm(range(395))
	while linea:
		separado = linea.split(";")
		concept = consultas.obtener_formato_tabla_openMRS("concept")
		concept["datatype_id"] = diccionarioTipoDeDato().get(separado[3],3)
		concept["class_id"] = 7 #Id de la clase Question
		concept["creator"] = 1
		concept["uuid"] = "'" + str(uuid.uuid1()) + "'"
		concept["date_created"] = "'" + time.strftime('%Y-%m-%d %H:%M:%S') + "'"
		idConcept =  consultas.insertarEnTabla("concept",concept,"CSV Conceptos",cont+1)
		if idConcept != 0:
			concept_name = consultas.obtener_formato_tabla_openMRS("concept_name")
			concept_name["concept_id"] = idConcept
			concept_name["creator"] = 1
			concept_name["name"] = ("'" + prefijo(linea) + separado[2] + "'").upper()
			concept_name["locale"] = "'es'"
			concept_name["uuid"] = "'" + str(uuid.uuid1()) + "'"
			concept_name["date_created"] = "'" + time.strftime('%Y-%m-%d %H:%M:%S') + "'"
			idConcept_name = consultas.insertarEnTabla("concept_name",concept_name,"CSV Conceptos",cont+1)

		linea = archivo.readline()
		cont += 1
		barraProgreso.update(1)
		barraProgreso.refresh()
	barraProgreso.close()